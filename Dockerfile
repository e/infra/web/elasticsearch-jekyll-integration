FROM python:3.9.6-alpine3.13 AS py

EXPOSE 5000

WORKDIR /usr/src/app

RUN apk add --update alpine-sdk && \
apk add libffi-dev openssl-dev && \
apk --no-cache --update add build-base

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY src .

CMD [ "uwsgi", "--ini", "config.ini" ]
