# Elastic-Jekyll

Full-text search for your Jekyll blog with ElasticSearch.

## Installation
1. Clone the project
1. run `python3 -m venv venv` to create the virtual env
1. run `. venv/bin/activate` to be able to use the python packages
1. run `pip install -r requirements.txt` to install all packages
1. Update the file `./src/config.ini` according to your needs

To be able to use the linting inside the virtual environment, it is recomended to use the python inside the virtual env folder. Take a look at [here](https://code.visualstudio.com/docs/python/environments#_select-and-activate-an-environment) for vscode

This project uses [pylint](https://pylint.org/) to help with the codestyle. If you wish to save some server resources, you can put the following shell script into the `.git/hooks/pre-commit` file (create one if it does not exists):
```shell
. venv/bin/activate
pylint --load-plugins pylint_quotes ./src
```

So every time you try to commit something, it will validate the linter locally beforehand.

## Development
1. You need a running copy of elasticsearch:
```bash
docker run \
--name elasticsearch \
--rm \
-p 9200:9200 \
-p 9300:9300 \
-e "discovery.type=single-node" \
elasticsearch:7.13.3
```
1. Make sure you are at the virtual env (run `. venv/bin/activate`)
1. To index your content, run: `python src/main.py "PATH_TO_YOUR_CONTENT"`
1. run `export FLASK_ENV=development`
1. run `export FLASK_APP=src/app`
1. run `flask run`


## Features

 - Parses the html from your Jekyll `_site` directory using BeautifulSoup to get more accurate content instead of using the raw Markdown.
 - Automates the process of updating your ElasticSearch index to store the post title, url and body, ready for querying from your site.
 - Searching on the generated ElasticSearch index which can be hosted on a server and then called by Javascript on your Jekyll blog to retrieve results.

## Example Usage

### Indexing:

 - Make sure you have an ElasticSearch server running. If not local, change the config in `indexer.py` to reflect your location.
 - Run the command `python src/main.py <path_to_blog>`, running without an argument will assume your compiled blog is located at `~/blog/_site`.
 - If the library cannot find your content correctly, modify `indexer.py` to point to the correct HTML elements for title, post content etc (assuming you have unique CSS classes for these).

### Searching:

 - See sample query on the ElasticSearch index in the  `searcher.py` script.
 - Results include the url to the post, the post title and a highlight of the relevant text that can be shown to the user.

 ### When to run

 Due the fact that the library relies on the generated output within the `_site` directory, you will need to re-run the indexer *after* you have re-built your blog when making changes. This unfortunately means that we cannot use something like Git webhooks to further automate the process, however it is still easy when put inside a script to execute after your site is built.


### Deployment Steps
Refer to the [deployment steps](https://gitlab.e.foundation/e/documentation/user/-/blob/master/docs/elasticsearch-jekyll-integration.md?ref_type=heads) for detailed instructions on how to proceed with the deployment.