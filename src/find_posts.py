""" Parse the content from html """
import glob
import logging
from bs4 import BeautifulSoup
from post import Post

DEFAULT_LANG = 'en'


def find_post_paths(base_dir):
    """ Get all html files inside a folder """
    files = glob.glob(base_dir + '/**/*.html', recursive=True)
    files = [f.replace('\\', '/') for f in files]
    return files


def get_title_from_htmltree(htmltree: BeautifulSoup):
    """ Grab the title from a set of possible places """
    title = htmltree.select_one('[data-elasticsearch-title]')
    if title is None:
        title = htmltree.find('h1', {'class': 'post-title'})
    if title is None:
        title = htmltree.find('h1')
    if title is None:
        return None
    return title.text.strip()


def get_subtitles_from_htmltree(htmltree: BeautifulSoup):
    """ Grab the subtitles from all h2,h3,h4,h5,h6 elements """
    subtitles = []
    headings = htmltree.select('h2,h3,h4,h5,h6')
    for heading in headings:
        subtitles.append(heading.text.strip())

    return subtitles


def get_body_from_htmltree(htmltree: BeautifulSoup):
    """ Get the body content of the page """
    post_elem = htmltree.select_one('[data-elasticsearch-body]')

    if post_elem is None:
        post_elem = htmltree.find('div', {'class': 'post'})
        if post_elem is None:
            return None
        post_elem.find(attrs={'class': 'post-title'}).decompose()
        post_elem.find(attrs={'class': 'post-date'}).decompose()

    paras = post_elem.find_all(text=True)

    body = ' '.join(p.strip() for p in paras).replace('  ', ' ').strip()

    return body


def get_htmltree_from_file(path: str) -> BeautifulSoup:
    """ Parse an html file into a html struct """
    with open(path, encoding='utf8') as file:
        contents = file.read()
        return BeautifulSoup(contents, 'html.parser')


def get_lang_from_htmltree(htmltree: BeautifulSoup) -> str:
    """ Returns the language set in the html tag """
    html = htmltree.select_one('html')
    lang = html.get('lang')
    return DEFAULT_LANG if lang is None else lang


def get_description_from_htmltree(htmltree: BeautifulSoup) -> str:
    """ Gather the description of the page from the meta tag description """
    metatag = htmltree.select_one('meta[name=\'description\']')
    if metatag is None:
        return None
    description = metatag.get('content')
    return description


def should_crawl_page(htmltree: BeautifulSoup) -> bool:
    """ Determines if the page should be crawled """

    metatag = htmltree.select_one('meta[name=\'robots\']')
    if metatag is None:
        return True

    metatag_content = metatag.get('content')
    if metatag_content is None:
        return True
    options = metatag_content.split(',')

    if 'noindex' in options:
        return False
    return True


def create_posts(base_dir):
    """ Returns a list posts crawled from the html content """
    paths = find_post_paths(base_dir)
    posts = []
    for path in paths:

        htmltree = get_htmltree_from_file(path)

        should = should_crawl_page(htmltree)
        if not should:
            continue

        title = get_title_from_htmltree(htmltree)
        if title is None:
            logging.warning('No element for title found in "%s"', path)
            continue
        body = get_body_from_htmltree(htmltree)
        if body is None:
            logging.warning('No element for body found in "%s"', path)
            continue

        subtittles = get_subtitles_from_htmltree(htmltree)

        description = get_description_from_htmltree(htmltree)
        if description is None:
            description = body

        lang = get_lang_from_htmltree(htmltree)

        page_id = path.replace(base_dir, '').replace('/', '-')
        url = path.replace(base_dir, '')

        posts.append(Post(
            post_id=page_id,
            title=title,
            subtittles=subtittles,
            url=url,
            body=body,
            description=description,
            lang=lang
        ))

    return posts
