""" Post module """
class Post: # pylint: disable=R0903
    """ Post data model """
    def __init__(self, post_id, title, subtittles, url, body, description, lang): # pylint: disable=R0913
        self.post_id = post_id
        self.title = title
        self.subtittles = subtittles
        self.url = url
        self.body = body

        if len(description) > 200:
            description = description[0:200] + '...'
        self.description = description

        self.lang = lang
