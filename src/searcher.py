""" Search module to interact with elastic search """
import math
from typing import List, Optional
from elasticsearch.client import Elasticsearch
import post
import indexer


def get_current_hash_by_language(
        elastic_search: Elasticsearch, language: str) -> Optional[str]:
    """ Get the oldest hash of the parsed folder using the language """
    all_indexes_in_language = [
        index for index in elastic_search.indices.get('*') if index.startswith(language)]

    if len(all_indexes_in_language) == 0:
        return None

    oldest_index = get_oldest_index(elastic_search, all_indexes_in_language)

    if oldest_index is None:
        return None

    return oldest_index.split('-')[1]


def get_current_hash(elastic_search: Elasticsearch) -> Optional[str]:
    """ Returns the hash to be used on the search """
    oldest_index = get_oldest_index(
        elastic_search, list(elastic_search.indices.get('*')))

    if oldest_index is None:
        return None

    oldest_index_params = oldest_index.split('-')

    if len(oldest_index_params) != 2:
        print(f'Your index "{oldest_index}" is not a valid index name')
        return None

    return oldest_index_params[1]


def get_oldest_index(elastic_search: Elasticsearch, indexes: List[str]) -> Optional[str]:
    """ Get the oldest indext inside the list """
    if len(indexes) == 0:
        return None
    oldest_index = None
    oldest_index_date = math.inf

    for i in indexes:

        if not elastic_search.indices.exists(i):
            continue

        index = elastic_search.indices.get(i)
        created_date = int(index[i]['settings']['index']['creation_date'])

        is_older = oldest_index_date > created_date
        if is_older:
            oldest_index_date = created_date
            oldest_index = i

    return oldest_index


def search_especific_query(
    elastic_search: Elasticsearch,
    user_query: str,
    language: str) -> Optional[post.Post]:
    """ Performs a more specific search to give priority to titles """

    index_name = get_index_name_from_lang(
        elastic_search=elastic_search, language=language)
    if index_name is None:
        return None

    query = {
        'query': {
            'bool': {
                'should': [
                    {
                        'match': {
                            'subtitles.keyword': {
                                'query': user_query,
                                'fuzziness': 0,
                                'max_expansions': 1,
                                'minimum_should_match': '100%',
                            },
                        }
                    },
                    {
                        'match': {
                            'title.keyword': {
                                'query': user_query,
                                'fuzziness': 0,
                                'max_expansions': 1,
                                'minimum_should_match': '100%',
                            },
                        }
                    }
                ]
            },
        },
        'highlight': {
            'fields': {
                'description': {}
            }
        },
        '_source': ['title', 'subtitles', 'url', 'description', 'lang', 'body']
    }

    res = elastic_search.search(index=index_name, body=query)

    if len(res['hits']['hits']) == 0:
        return None

    for hit in res['hits']['hits']:
        return _from_hit_to_post(hit)

    return None


def _from_hit_to_post(hit) -> post.Post:
    result = hit['_source']
    if 'highlight' in hit:
        result['description'] = ' '.join(hit['highlight']['description'])
    return result


def search_query(elastic_search: Elasticsearch, user_query: str, language: str) -> List[post.Post]:
    """ Performs the search using a query on a specific language """

    index_name = get_index_name_from_lang(
        elastic_search=elastic_search, language=language)
    if index_name is None:
        return []

    query = {
        'query': {
            'multi_match': {
                'query': user_query,
                'type': 'best_fields',
                'fuzziness': 'AUTO',
                'tie_breaker': 0.3,
                'fields': ['title^10', 'subtitles^9', 'description^2', 'body'],
            }
        },
        'highlight': {
            'fields': {
                'description': {}
            }
        },
        '_source': ['title', 'subtitles', 'url', 'description', 'lang', 'body']
    }

    res = elastic_search.search(index=index_name, body=query)
    results = []
    for hit in res['hits']['hits']:
        result = _from_hit_to_post(hit)
        results.append(result)
    return results


def autocomplete(elastic_search: Elasticsearch, language: str) -> List[str]:
    """ Get the Autocomplete list  """

    index_name = get_index_name_from_lang(
        elastic_search=elastic_search, language=language)
    if index_name is None:
        return []
    return autocomplete_for_index(elastic_search=elastic_search, index_name=index_name)


def get_index_name_from_lang(elastic_search: Elasticsearch,
                             language: str) -> Optional[str]:
    """ Get the index name from a content language """
    current_hash = get_current_hash_by_language(elastic_search, language)
    if current_hash is None:
        return None

    index_name = indexer.get_index_name(language, current_hash)

    if not elastic_search.indices.exists(index_name):
        return None

    return index_name


def autocomplete_for_index(elastic_search: Elasticsearch,
                           index_name: str) -> Optional[List[str]]:
    """ Get the autocomplete for a specific index """
    query = {
        'query': {
            'match_all': {}
        },
        'size': 10000,
        '_source': ['title', 'subtitles']
    }

    res = elastic_search.search(index=index_name, body=query)
    results = []
    for hit in res['hits']['hits']:
        results.append(hit['_source']['title'])
        results += hit['_source']['subtitles']

    if '' in results:
        results.remove('')

    results = list(set(results))
    return results


if __name__ == '__main__':
    import os
    from indexer import connect_elastic
    from dotenv import load_dotenv
    load_dotenv()
    es_host = os.getenv('ES_HOST', 'localhost')
    es_port = os.getenv('ES_PORT', '9200')

    es = connect_elastic(es_host, es_port)
    print(search_query(es, 'map', 'en'))

# POST /blog/post/_search
# {
#     'query': {
#       'multi_match': {
#         'query': 'python',
#         'type': 'best_fields',
#         'fuzziness': 'AUTO',
#         'tie_breaker': 0.3,
#         'fields': ['title^3', 'body']
#       }
#     },
#     'highlight': {
#         'fields' : {
#             'body' : {}
#         }
#     },
#     '_source': ['title', 'url']
# }
