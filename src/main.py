""" Indexer entry point """
import sys
import os
from hashlib import md5
from dotenv import load_dotenv
import indexer
from find_posts import create_posts
import searcher

load_dotenv()


def index_folder(base_dir: str):
    """ Performs the indexation of the folder """
    if not os.path.exists(base_dir):
        print(f'No folder was found at {base_dir}')
        return

    es_host = os.getenv('ES_HOST', 'localhost')
    es_port = os.getenv('ES_PORT', '9200')

    print(f'Connecting to elastic search at: {es_host}:{es_port}')

    elastic_search = indexer.connect_elastic(es_host, es_port)
    print('ElasticSearch connection established')
    try:
        base_dir_encoded = base_dir.encode()
        folder_hash = md5(base_dir_encoded).hexdigest()
        current_hash = searcher.get_current_hash(elastic_search)
    except UnicodeEncodeError as encode_error:
        print(f'Error encoding base_dir: {encode_error}')
        return
    if folder_hash == current_hash:
        print(f'The folder {base_dir} was already indexed. Hash: {folder_hash}')
        return

    print('Finding posts in', base_dir)

    posts = create_posts(base_dir)
    print(f'Posts created ({len(posts)})')

    unique_languages = set(post.lang for post in posts)

    indexer.create_indexes(elastic_search, unique_languages, folder_hash)
    indexer.index_posts(elastic_search, posts, folder_hash)
    print('Finished indexing posts')

    print(f'Deleting all indexes except {folder_hash}')
    indexer.delete_all_indexes_except(elastic_search, folder_hash)


if __name__ == '__main__':
    # provide blog base directory as arg
    if len(sys.argv) != 2:
        raise BaseException(
            'You must pass the project folder to be crawled, and only it.')

    BASE_DIRECTORY = str(sys.argv[1])

    index_folder(BASE_DIRECTORY)
