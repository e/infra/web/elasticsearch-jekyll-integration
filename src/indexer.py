""" Indexer of posts """
from time import sleep
from typing import List
from elasticsearch import Elasticsearch
import post as PostModule


def connect_elastic(host='localhost', port=9200):
    """ Returns an connected elastic search instance with a small retry policy """
    maximum_attemps = 12
    connection = Elasticsearch([{'host': host, 'port': port}])
    for _ in range(maximum_attemps):
        is_connected = connection.ping()
        if not is_connected:
            sleep(5)
            continue

        return connection

    raise TimeoutError(
        f'Could not connect to elasticsearch server at {host}:{port}.')


def create_indexes(elastic_search: Elasticsearch, languages: List[str], unique_hash: str):
    """ Create the indexes for the languages using a unique hash """
    for lang in languages:
        index_name = get_index_name(lang, unique_hash)
        if not elastic_search.indices.exists(index=index_name):
            elastic_search.indices.create(index=index_name)


def get_index_name(language: str, unique_hash: str) -> str:
    """ Returns the index name of a language with the hash"""
    return f'{language}-{unique_hash}'


def delete_all_indexes_except(elastic_search: Elasticsearch, except_hash: str):
    """ Clear the elastic search instance of all content,
        with the exception of the content marked with the hash
    """
    for index_name in elastic_search.indices.get('*'):
        if except_hash not in index_name:
            elastic_search.indices.delete(index=index_name)


def index_posts(elastic_search: Elasticsearch, posts: List[PostModule.Post], unique_hash: str):
    """ Indexes the posts into a elastic search instance """
    # Define the specific URLs you want to exclude from indexing
    excluded_urls = [
        '/index.html',
        '/fr/index.html',
        '/es/index.html',
        '/de/index.html',
        '/it/index.html',
    ]
    for post in posts:
        # Check if the post URL is in the list of excluded URLs; if so, skip indexing this post
        if post.url in excluded_urls:
            continue
        doc = {
            'title': post.title,
            'subtitles': post.subtittles,
            'url': post.url,
            'description': post.description,
            'body': post.body,
            'lang': post.lang,
        }

        index_name = get_index_name(post.lang, unique_hash)

        elastic_search.index(index=index_name, id=post.post_id, body=doc)
        print('Created doc for', post.url)
