""" Handles connection with the outside world by REST api """
import gzip
import os
from flask import Flask, json, Response, make_response
from dotenv import load_dotenv
import searcher
from indexer import connect_elastic

app = Flask(__name__)

load_dotenv()

es_host = os.getenv('ES_HOST', 'localhost')
es_port = os.getenv('ES_PORT', '9200')

es = connect_elastic(es_host, es_port)


@app.route('/<language>/search/<query>')
def search(language, query):
    """ Endpoint to search into all contents of the specific language """
    specific_post = searcher.search_especific_query(es, query, language)
    result = searcher.search_query(es, query, language)
    data = {
        'search_results': result,
        'specific_post': specific_post,
    }
    try:
        resp = gzip_json_response(data)
    except BaseException: # pylint: disable=w0703
        resp = json.dumps({'error': 'Unable to process at the moment'})
        headers = {
            'Access-Control-Allow-Origin': os.getenv('ALLOW_ORIGINS', '*')
        }
        return Response(response=resp,
                        status=500,
                        content_type='application/json',
                        headers=headers)
    resp.headers['Access-Control-Allow-Origin'] = os.getenv(
        'ALLOW_ORIGINS', '*')
    return resp


@app.route('/<language>/autocomplete')
def autocomplete(language):
    """ Endpoint to return a list of autocomplete words """
    result = searcher.autocomplete(es, language)
    data = {
        'autocomplete': result,
    }
    return gzip_json_response(data)


def gzip_json_response(data: dict) -> Response:
    """ Converts a dictionary into a flask response with it's content compressed (gzip) """
    content = gzip.compress(json.dumps(data).encode('utf8'), 5)
    response = make_response(content)
    response.headers['Content-length'] = len(content)
    response.headers['Content-Encoding'] = 'gzip'
    response.headers['Content-Type'] = 'application/json'
    return response
